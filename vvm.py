from cloud_definition import *
from Parameter import Parameter 
from Parameter import Path
from Parameter import Ncpath
from IOManager import IOManager

import sys
import re
import math
import pdb
import netCDF4
import numpy as np
import _pickle as pickle
import multiprocessing as mp
import os.path
import time as systime
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import seaborn as sns


# The process of Tracking
class Tracking:
  
  def find_cloud(time) :
    # This program can be the Time-Seperated MultiThread Program
    print("Handle Con at time = %6d" %(time) )
    # Get Required Variable
    qc   =  IOManager.getNC('qc',time)
    qi   =  IOManager.getNC('qi',time)
    qv   =  IOManager.getNC('qv',time)
    th   =  IOManager.getNC('th',time)
    u    =  IOManager.getNC('u',time)
    v    =  IOManager.getNC('v',time)
    w    =  IOManager.getNC('w',time)
    xi   =  IOManager.getNC('xi',time)
    eta  =  IOManager.getNC('eta',time)
    zeta =  IOManager.getNC('zeta',time)
    zc   =  IOManager.getNC_1D('zc')
    rho  =  Parameter.RHO

    # Derived Data Array
    bouy        =  np.zeros((Parameter.NZ,Parameter.NY,Parameter.NX)) # Bouyancy
    projectXY   =  np.zeros((Parameter.NY,Parameter.NX)) # Calculate Covering
    search_flag =  np.zeros((Parameter.NZ,Parameter.NY,Parameter.NX))
    label_data  =  np.zeros((Parameter.NZ,Parameter.NY,Parameter.NX))
    rec_data    =  [] 
    # Function Scale Variable
    label       =  1
    
    # Calculate Buoy
    for k in range(Parameter.NZ):
      avg_th = 0.0
      for j in range(Parameter.NY):
        for i in range(Parameter.NX):
          theta_p = th[k,j,i] * ( 1+0.608*qv[k,j,i]-qc[k,j,i]-qi[k,j,i] )
          avg_th  = avg_th + theta_p
      avg_th = avg_th / Parameter.NX / Parameter.NY
      for j in range(Parameter.NY):
        for i in range(Parameter.NX):
          theta_p = th[k,j,i] * ( 1+0.608*qv[k,j,i]-qc[k,j,i]-qi[k,j,i] )
          avg_th  = avg_th + theta_p
          bouy[k,j,i] = 9.81 * ( theta_p - avg_th ) / avg_th 
    
    # Cloud Connecting Algorithm
    for k in range(Parameter.NZ):
      for j in range(Parameter.NY):
        for i in range(Parameter.NX):
          if ( search_flag[k,j,i] == 1):
            continue
          
          # Initialize
          stackPos = [[k,j,i]] # [z,y,x] Stack For Searching 
          search_flag[k,j,i] = 1
          cloud = Cloud() 

          # Connecting Cloud
          while ( len(stackPos) != 0 ):
            cuPos = stackPos.pop()
            cuZ   = cuPos[0]
            cuY   = cuPos[1]
            cuX   = cuPos[2]
            
            # The definition of Cloudy grid 
            if ( qc[cuZ,cuY,cuX] + qi[cuZ,cuY,cuX] < 1.0E-5 ):
              continue

            label_data[cuZ,cuY,cuX] = label
            cloud.total_grid += 1

            if ( cuZ != 0 ):
              cuVol = Parameter.DX * Parameter.DY * ( zc[cuZ] - zc[cuZ-1] )
              if ( cloud.h_max < zc[cuZ] ):
                cloud.h_max = zc[cuZ]
              if ( cloud.h_min > zc[cuZ] ):
                cloud.h_min = zc[cuZ]
            else :
              cuVol = Parameter.DX * Parameter.DY * ( zc[1] - zc[0] )
              if ( cloud.h_max < zc[1] ):
                cloud.h_max = zc[1]
              if ( cloud.h_min > zc[1] ):
                cloud.h_min = zc[1]

            # Increase the Cloud Object physical parameter
            cloud.volume     += cuVol
            cloud.qc_sum     += cuVol * qc[cuZ,cuY,cuX] * rho[cuZ]
            cloud.qi_sum     += cuVol * qi[cuZ,cuY,cuX] * rho[cuZ]
            cloud.bouy_sum   += bouy[cuZ,cuY,cuX] ** 2 
            cloud.VKE_sum    += 0.5 * w[cuZ,cuY,cuX] ** 2
            cloud.enstrophy  += 0.5 * (
              xi[cuZ,cuY,cuX]**2 + eta[cuZ,cuY,cuX]**2 + zeta[cuZ,cuY,cuX]**2 
            )
            cloud.mass_flux  += rho[cuZ] * Parameter.DX * Parameter.DY * w[cuZ,cuY,cuX]
            cloud.massInt    += rho[cuZ] * cuVol * w[cuZ,cuY,cuX]
            cloud.center_x   += float(cuX) * ( cuVol * (qc[cuZ,cuY,cuX]+qi[cuZ,cuY,cuX]) * rho[cuZ] )
            cloud.center_y   += float(cuY) * ( cuVol * (qc[cuZ,cuY,cuX]+qi[cuZ,cuY,cuX]) * rho[cuZ] )
            cloud.center_z   += float(cuZ) * ( cuVol * (qc[cuZ,cuY,cuX]+qi[cuZ,cuY,cuX]) * rho[cuZ] )
            cloud.u_average  += abs( u[cuZ,cuY,cuX] ) 
            cloud.v_average  += abs( v[cuZ,cuY,cuX] ) 
            if ( projectXY[cuY,cuX] != label ):
              projectXY[cuY,cuX] = label
              cloud.proArea += Parameter.DX * Parameter.DY

            for move in range(Parameter.CONNECT) :
              # Obtain the position of next move
              neZ = ( cuZ + Parameter.MOVE[move][0] )
              neY = ( cuY + Parameter.MOVE[move][1] + Parameter.NY ) % Parameter.NY
              neX = ( cuX + Parameter.MOVE[move][2] + Parameter.NX ) % Parameter.NX
              if ( neZ >= Parameter.DZ or neZ <= 0 ):
                continue
              if ( search_flag[neZ,neY,neX] == 1 ):
                continue
              
              stackPos.append([neZ,neY,neX]) 
              search_flag[neZ,neY,neX] = 1
         
          if ( cloud.volume >= 1.0E-8 ) :
            # Preprocess before Write Record
            cloud.label      =  label
            cloud.massInt   /=  ( cloud.h_max - cloud.h_min + 50 ) # Prevent Inf
            cloud.center_x  /=  ( cloud.qc_sum + cloud.qi_sum )
            cloud.center_y  /=  ( cloud.qc_sum + cloud.qi_sum )
            cloud.center_z  /=  ( cloud.qc_sum + cloud.qi_sum )
            cloud.u_average /=  float(cloud.total_grid)
            cloud.v_average /=  float(cloud.total_grid)
            # Write Record of the Cloud Object
            rec_data.append(cloud)
            label += 1

          del cloud
    return rec_data

  def connect_subProcess(queue,processID,timeList):
    # The process that control subprocess to call find_cloud
    if ( timeList ) :
      rec_dataList = []
      for time in timeList:
        rec_dataList.append([Tracking.find_cloud(time),time])
      queue.put(rec_dataList)
    else:
      queue.put(0)

  def makeCloudConFile(timeRange = [0,Parameter.NT - 1]):
    # Connect Cloud at each time of output
    #   CloudCon 
    #   [[] = not searched time , [0] = searched but no cloud , [Cloud1,Cloud2...] ]
    #   Check if the cloud.pickle exist, avoid same calculation

    # TODO : need to check whether pre-exist file is same to the current nc files, by NT DX DY or other methods
    # TODO : make this function be called anytime when tracked called, and work if required time is not searched time

    print ("Pre-Reading")
    cloudFile = Path.out_path + Path.out_pickle
    if ( os.path.isfile(cloudFile) ) : 
      with open(Path.out_path + Path.out_pickle ,mode='rb') as fp:
        unpickler = pickle.Unpickler(fp);
        cloudCon = unpickler.load();
    else:
      cloudCon = [ [] for i in range(Parameter.NT) ]
    
    # Multi-process handling
    print ("Multi processing")
    queue = mp.Queue()
    processTimeList = [ [] for i in range(Parameter.NT) ]
    
    indexCount = 0
    for time in range(timeRange[0],timeRange[1]+1):
      if ( cloudCon[time] == [] ):
        processTimeList[ indexCount % Path.PROCNUM  ].append(time)
        indexCount += 1
    process = []
    for proc in range(Path.PROCNUM):
      process.append( 
        mp.Process( 
          target=Tracking.connect_subProcess ,
          args=( queue , proc , processTimeList[proc] ) 
        )
      )
    for ps in process :
      ps.start()
  
    # Get the result from processes
    print ("Waiting Recycle")
    for ps in range(Path.PROCNUM):
      returnQ = queue.get()
      if ( not returnQ == 0 ):
        for data in returnQ :
          if ( data[0] == [] ) :
            cloudCon[ data[1] ].append(0)  # [ [0] ]
          else:
            cloudCon[ data[1] ].extend( data[0] )  #[ [ Cloud1,Cloud2...  ] ]

    with open(Path.out_path + Path.out_pickle,mode='wb') as fp:
      pickle.dump(cloudCon,fp)

  def makeACCloudFile(timeRange):
    # Get Aggregated Convection Cloud Files 

    # Connect Cloud at each time of output
    #   CloudCon 
    #   [[] = not searched time , [0] = searched but no cloud , [Cloud1,Cloud2...] ]
    #   Check if the cloud.pickle exist, avoid same calculation
    print ("Pre-Reading")
    accloudFile = Path.out_path + Path.out_acpickle
    if ( os.path.isfile(accloudFile) ) : 
      with open(Path.out_path + Path.out_acpickle ,mode='rb') as fp:
        unpickler = pickle.Unpickler(fp);
        accloudCon = unpickler.load();
    else:
      accloudCon = [ [] for i in range(Parameter.NT) ]
    
    # Multi-process handling
    print ("Multi processing")
    queue = mp.Queue()
    processTimeList = [ [] for i in range(Parameter.NT) ]
    
    indexCount = 0
    for time in range(timeRange[0],timeRange[1]+1):
      if ( accloudCon[time] == [] ):
        processTimeList[ indexCount % Path.PROCNUM  ].append(time)
        indexCount += 1
    process = []
    for proc in range(Path.PROCNUM):
      process.append( 
        mp.Process( 
          target=Tracking.subProcess ,
          args=( queue , proc , processTimeList[proc] ) 
        )
      )
    for ps in process :
      ps.start()
  
    # Get the result from processes
    print ("Waiting Recycle")
    for ps in range(Path.PROCNUM):
      returnQ = queue.get()
      if ( not returnQ == 0 ):
        for data in returnQ :
          if ( data[0] == [] ) :
            cloudCon[ data[1] ].append(0)  # [ [0] ]
          else:
            cloudCon[ data[1] ].extend( data[0] )  #[ [ Cloud1,Cloud2...  ] ]

    with open(Path.out_path + Path.out_pickle,mode='wb') as fp:
      pickle.dump(cloudCon,fp)

  def readCloudCon():
    startTime = systime.time()
    print("Reading Cloud Con")
    with open(Path.out_path + Path.out_pickle ,mode='rb') as fp:
      unpickler = pickle.Unpickler(fp)
      cloudCon = unpickler.load()
    print("End Reading Cloud Con, Time Used = %d [sec]" %(systime.time()-startTime))
    return cloudCon

  def track_cloud(time = [ 0 , Parameter.NT - 1 ]):
    # Track the clouds, connecting the cloud between outputs
    # Input  : [Starting Time, Ending Time] , Default value : first and last output time 
    # Output : [List of heads of every tracked clouds]
    
    # TODO   : Use filename or something to indicate the searched Time period , like "TrackedHead_56_89.pickle"

    # Measure time
    startTime = systime.time()

    # Tracking Cloud from different time
    trackedCloudLib = []
    cloudCon = Tracking.readCloudCon()

    # Naive Nearest Neighbor Searching
    print("Start Tracking Cloud")

    cuExistCloudDict   = None
    candExistCloudDict = None
    for t in range(time[0],time[1]) :
      print("Track with %5d current clouds in Time = %5d , and %7d future clouds in Time = %5d " % (len(cloudCon[t]),t,len(cloudCon[t+1]),t+1))
      
      # Create two Dicts to hold the traked clouds
      if ( cuExistCloudDict is None ):
        cuExistCloudDict = dict()
      candExistCloudDict = dict()

      # From  0 to n-2 , process from 0 to n-1
      for cuCloud in cloudCon[t] :
        
        if ( cuCloud == 0 or cuCloud is None ):
          # 0    : no cloud connected
          # None : Have not be looked up by program
          continue

        if ( cuCloud.volume <= Parameter.DX * Parameter.DY * Parameter.DZ1 * 27 ) :
          # Block the cloud too small
          continue


        # print("Current Cloud : Time = %d , volume = %10.3f [km^3] " % (t,cuCloud.volume/1000000000))

        # cuCloud : The cloud object that get from Cloud Connecting Function
        cuX = cuCloud.center_x
        cuY = cuCloud.center_y
        cuZ = cuCloud.center_z
        cuV = cuCloud.v_average
        cuU = cuCloud.u_average

        cuMin = 10000000000000 # Decide the largest distance for two clouds
        candCloud = None # Candidate of the next cloud

        # Find the nearest Cloud in the set of clouds in next steps 


        for fuCloud in cloudCon[t+1] :
          fuX = fuCloud.center_x
          fuY = fuCloud.center_y
          fuZ = fuCloud.center_z
          distCuFu = (
            ( (fuX - cuX) * Parameter.DX ) ** 2
            + ( (fuY - cuY) * Parameter.DY ) ** 2
            + ( (fuZ - cuZ) * Parameter.DZ ) ** 2
          ) ** 0.5 

          # change the cloud tracking searching distance here
          if( 
              ( cuMin > distCuFu ) and
              ( distCuFu < (Parameter.DX ** 2 + Parameter.DY ** 2) ** 0.5 * 5 
                + ( cuCloud.volume / ( cuCloud.h_max - cuCloud.h_min ) ) ** 0.5 
              )
          ) :
            candCloud = fuCloud
            cuMin = distCuFu
        
        # Record tracked cloud into Dict
        if( candCloud is not None ):


          # Record the cloud that has at least exists in two time
          if ( hash(cuCloud) in cuExistCloudDict ):
            # This cloud has previous record
            cuTracked = cuExistCloudDict[hash(cuCloud)]
            
            if ( hash(candCloud) in candExistCloudDict ) :
              # merge 
              fuTracked = candExistCloudDict[hash(candCloud)]
            else :
              # No merge
              fuTracked            = TrackedCloud()
              fuTracked.cloud      = candCloud
              fuTracked.time       = t+1

              candExistCloudDict[hash(candCloud)] = fuTracked 


            cuTracked.nexTracked.append(fuTracked)
            fuTracked.preTracked.append(cuTracked)
            fuTracked.head.extend(cuTracked.head)

            # Record the maximum volume and propagate to all the record

          else:
            # This cloud doesn't have previous record
            cuTracked            = TrackedCloud()
            cuTracked.cloud      = cuCloud
            cuTracked.time       = t
            cuTracked.maxVolume  = cuCloud.volume
            cuTracked.head       = []

            fuTracked            = TrackedCloud()
            fuTracked.cloud      = candCloud
            fuTracked.time       = t+1

            cuTracked.nexTracked.append(fuTracked)
            fuTracked.preTracked.append(cuTracked)
            fuTracked.head.append(cuTracked)

            cuExistCloudDict[hash(cuCloud)]     = cuTracked
            candExistCloudDict[hash(candCloud)] = fuTracked

            # Add head of one tracked cloud
            trackedCloudLib.append(cuTracked)

          maxVolume = 0.0
          for head in fuTracked.head :
            maxVolume = max(head.maxVolume,maxVolume)
          maxVolume = max( fuTracked.cloud.volume , maxVolume )

          # Update all the path with this new value
          for head in fuTracked.head:
            cuP = head
            if ( cuP.maxVolume == maxVolume ):
              continue             
            while True:
              cuP.maxVolume = maxVolume
              if ( cuP.nexTracked == [] ):
                break
              else :
                cuP = cuP.nexTracked[0]

      # Iterate the cloud library
      cuExistCloudDict   = None
      cuExistCloudDict   = candExistCloudDict
      candExistCloudDict = None 
      
    print("End Tracking Cloud, Time Used = %d [sec]" %(systime.time()-startTime))
    with open(Path.out_path + Path.out_trackHead,mode='wb') as fp:
      pickle.dump(trackedCloudLib,fp)
    return trackedCloudLib

  def readTrackedCloud():
    startTime = systime.time()
    print("Reading Tracked Head")
    with open(Path.out_path + Path.out_trackHead ,mode='rb') as fp:
      unpickler = pickle.Unpickler(fp)
      trackedHead = unpickler.load()
    print("End Reading Tracked Head, Time Used = %d [sec]" %(systime.time()-startTime))
    return trackedHead

class CloudPlot:

  # Common Used Function 
  def __init__(self):
    self.cloudCon     = Tracking.readCloudCon()
    self.cloudNum     = []

  def getCloudNum(self):
    if ( self.cloudNum == [] ):
      for i in range(Parameter.NT):
        self.cloudNum.append( len( self.cloudCon[i] ) )
      return self.cloudNum
    else:
      return self.cloudNum
  def titleAdd(self,plt,xtitle=None,ytitle=None,xlim=None,ylim=None):
    print(plt.title("Cloud Number"))

  # Plot Function 
  def plot_time_num(self,time):
    # Plot figure : Num vs Time
    num = self.getCloudNum()
    cs = plt.plot(
      range(time[0],time[1]),
      num[time[0]:time[1]]
    )
    #self.titleAdd(plt=cs)
    plt.show()
    plt.savefig('temp.png')
  
  def plot_cloud_size_Overlap(time):
    print("Plot cloud size Overlap")
    trackHead = Tracking.track_cloud(time)
    sizeOverlap = []
    maxlen = 0
    for head in trackHead:
      cuSize = []
      cuTrackedCloud = head
      listLen = 0
      while ( cuTrackedCloud is not None ):
        cuSize.append( cuTrackedCloud.cloud.volume / 1000000000.0 )
        if ( len(cuTrackedCloud.nexTracked ) > 0 ):
          cuTrackedCloud = cuTrackedCloud.nexTracked[0]
        else:
          cuTrackedCloud = None
        listLen += 1

      sizeOverlap.append(cuSize)
      if ( maxlen < listLen ):
        maxlen = listLen

    avg = np.array([ 0  for j in range(maxlen) ])
    x   = np.arange(maxlen) * Parameter.ODT / 60.0
    all_y = []
    for i in range(len(trackHead)):
      y = np.array([ 0 for j in range(maxlen) ])
      y[0:len(sizeOverlap[i])] = sizeOverlap[i]
      if ( y.max() > 20.0 ):
        all_y.append(y)
        avg = avg + y
    avg = np.array(avg) / len(all_y)

    ax = sns.tsplot(all_y,time=x,ci=[68, 95, 99.7])
    ax.set_title("Cloud Life Time evolution")
    ax.set_xlabel("Life time [min]")
    ax.set_ylabel("Cloud Size [km^3]")
    ax.set_xlim([0,100])
    ax.set_ylim([0,100])
    fig = ax.get_figure()
    fig.savefig('temp.png')

  def draw_typhoon_graph():
    trackedHead    = Tracking.readTrackedCloud() 
    fig = plt.figure()
    ax  = fig.add_subplot(111,aspect='equal')
    trackedIndex   = [1078,1322,1665]#range(len(trackedHead))#[17,717,1535]#[0,32419,38260,38046,38327,37838,38461,38913,36601,38879]#[0,288,265,147,369,155,207]#range(len(trackedHead))

   
    i = 0
    for head in sorted(trackedHead , key= lambda head : head.maxVolume, reverse= True):
      print("Maximum volume of this tracked cloud is %15.5f[km^3] in index %d " %(head.maxVolume / 1000000000.0 , trackedHead.index(head)))
      #trackedIndex.append(trackedHead.index(head))

      i = i + 1
      if ( i > 10 ):
        break
    

    # Preprocess the data to visualize
    drawData = []
    drawDataCombine = [[],[],[],[]]
   
    for i in trackedIndex:
      head   = trackedHead[i]
      X      = []
      Y      = []
      T      = []
      while True:
        cu_x     = head.cloud.center_x
        cu_y     = head.cloud.center_y
        cu_t     = head.time
        cu_vol   = head.cloud.volume
        cu_thick = head.cloud.h_max - head.cloud.h_min
        X.append(cu_x)
        Y.append(cu_y)
        T.append(cu_t)

        drawDataCombine[0].append(cu_x)
        drawDataCombine[1].append(cu_y)
        drawDataCombine[2].append(cu_vol)
        drawDataCombine[3].append(cu_thick)

        if ( head.nexTracked == [] ):
          break
        else :
          head = head.nexTracked[0]
      drawData.append([X,Y,T])
      if (len(X) > 10 ):
        print ("len = %d , index = %d " %(len(X),i))
      #  drawData.append([X,Y,T])
    # pdb.set_trace()


    for i in range(len(drawData)):
      # Draw Lines
      plt.plot(drawData[i][0],drawData[i][1],'g-', linewidth = 0.5 )
      
      # Draw Red Circle marker at First, and red triangle at last 
      lastIndexOfTrack = len(drawData[i][0])-1
      plt.plot(drawData[i][0][0],drawData[i][1][0],'>', color = 'r', ms= 1)
      #plt.plot(drawData[i][0][lastIndexOfTrack],drawData[i][1][lastIndexOfTrack],'o',color='r', ms=1)
      
      # Draw Time at first track point
      plt.text(drawData[i][0][0],drawData[i][1][0],"T=%d" %(drawData[i][2][0]),ha='right',va='top',size=3)

    # Draw circle
    # add pseudo data to specify the circle size
    drawDataCombine[0].extend([Parameter.NX+5]*3)
    drawDataCombine[1].extend([5,25,45])
    drawDataCombine[2].extend([10**11,10**10,10**9]) # 100km^3 , 10km^3 , 1km^3
    drawDataCombine[3].extend([Parameter.DZ] *3)

    numpyDataArray = np.array(drawDataCombine)
    numpyDataArray[2][numpyDataArray[2]<10**4] = 10**4

    color = cm.jet(numpyDataArray[3])
    
    for i in range(len(drawDataCombine[0])):
      # Draw Circle
      circle = plt.Circle(
        (numpyDataArray[0][i],numpyDataArray[1][i]),
        #drawDataCombine[2][i] ** (1/3) / (Parameter.DX ** 2 + Parameter.DY ** 2) ** 0.5,
        np.log10(numpyDataArray[2][i]/10**4)*3,
        fill  =  False,
        color =  color[i],
        alpha = 0.3,
      )
      ax.add_patch(circle)
    
    
    # Draw Time at first track point
    plt.text(numpyDataArray[0][-3],numpyDataArray[1][-3],"[100km^3]"  ,ha='center',va='center',size=4)
    plt.text(numpyDataArray[0][-2],numpyDataArray[1][-2],"[10km^3]"   ,ha='center',va='center',size=4)
    plt.text(numpyDataArray[0][-1],numpyDataArray[1][-1],"[1km^3]"    ,ha='center',va='center',size=4)

    cmap_jet = cm.get_cmap('jet')
    sc = plt.scatter(numpyDataArray[0],numpyDataArray[1],c = numpyDataArray[3],s=0,cmap=cmap_jet)
    
    ax.set_xlim(0,Parameter.NX+20)
    ax.set_ylim(0,Parameter.NY+1)
    plt.xlabel('X grid')
    plt.ylabel('Y grid')
    plt.title('Cloud Tracking')
    plt.colorbar(sc)
    fig.savefig(Path.out_graph_path + 'typhoon.png', dpi=150)

  def example(self,time):
    num = getCloudNum()

    X,Y = np.meshgrid(
      np.linspace(0,256,256),
      np.linspace(0,50,50)
    )
    
    cs = plt.contourf(X,Y,getMap[0:50,30,0:256])
    cbar = plt.colorbar()
    plt.show()
    plt.savefig('temp.png')

if __name__ == '__main__':
  '''
  timeList = np.array(
    [
       [346,489]
      ,[490,650]
      ,[651,788]
      ,[789,930]
      ,[931,1079]
      ,[346,350]
    ]
  )

  CloudPlot.plot_cloud_size_Overlap(timeList[4]) 

  Tracking.setNC_3D()
  '''
  #Tracking.makeCloudConFile()
  #Tracking.track_cloud([80,144])
  CloudPlot.draw_typhoon_graph()  




