#PBS -l nodes=2:ppn=8
#PBS -N cloud_connecting
#PBS -o vvm.py.out
#PBS -e vvm.py.err
#PBS -k oe

WORKDIR=/home/flyingmars/vvmpython

cd $WORKDIR
/data2/flyingmars/miniconda3/bin/python vvm.py

