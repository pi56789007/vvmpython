from Parameter import Ncpath
from Parameter import Parameter
import sys
import netCDF4
import _pickle as pickle
import os.path

class IOManager:

  def checkRequiredPath( parameter ):
    mapDict = {
      'qc'  : Ncpath.qc ,
      'qg'  : Ncpath.qg ,
      'qi'  : Ncpath.qi ,
      'qr'  : Ncpath.qr ,
      'qs'  : Ncpath.qs ,
      'qv'  : Ncpath.qv ,
      'u'   : Ncpath.u  ,
      'v'   : Ncpath.v  ,
      'w'   : Ncpath.w  ,
      'th'  : Ncpath.th ,
      'xi'  : Ncpath.zdx,
      'eta' : Ncpath.zdy,
      'zeta': Ncpath.zdz,
      'xc'  : Ncpath.w  ,
      'yc'  : Ncpath.w  ,
      'zc'  : Ncpath.w  
    }

    if   ( parameter in mapDict  ) :
      return mapDict[parameter]
    else:
      print("The variable doesn't exist")
      sys.exit(1) 

  def ncdump(nc_fid, verb=False):
    '''
    ncdump outputs dimensions, variables and their attribute information.
    The information is similar to that of NCAR's ncdump utility.
    ncdump requires a valid instance of Dataset.

    Parameters
    ----------
    nc_fid : netCDF4.Dataset
      A netCDF4 dateset object
    verb : Boolean
      whether or not nc_attrs, nc_dims, and nc_vars are printed

    Returns
    -------
    nc_attrs : list
      A Python list of the NetCDF file global attributes
    nc_dims : list
      A Python list of the NetCDF file dimensions
    nc_vars : list
      A Python list of the NetCDF file variables
    '''
    def print_ncattr(key):
      """
      Prints the NetCDF file attributes for a given key

      Parameters
      ----------
      key : unicode
        a valid netCDF4.Dataset.variables key
      """
      try:
        print ("\t\ttype:", repr(nc_fid.variables[key].dtype) )
        for ncattr in nc_fid.variables[key].ncattrs():
          print ('\t\t%s:' % ncattr,\
              repr(nc_fid.variables[key].getncattr(ncattr)))
      except KeyError:
        print ("\t\tWARNING: %s does not contain variable attributes" % key)

    # NetCDF global attributes
    nc_attrs = nc_fid.ncattrs()
    if verb:
      print ("NetCDF Global Attributes:")
      for nc_attr in nc_attrs:
        print ('\t%s:' % nc_attr, repr(nc_fid.getncattr(nc_attr)))
    nc_dims = [dim for dim in nc_fid.dimensions]  # list of nc dimensions
    # Dimension shape information.
    if verb:
      print ("NetCDF dimension information:")
      for dim in nc_dims:
        print ("\tName:", dim )
        print ("\t\tsize:", len(nc_fid.dimensions[dim]))
        print_ncattr(dim)
    # Variable information.
    nc_vars = [var for var in nc_fid.variables]  # list of nc variables
    if verb:
      print ("NetCDF variable information:")
      for var in nc_vars:
        if var not in nc_dims:
          print ('\tName:', var)
          print ("\t\tdimensions:", nc_fid.variables[var].dimensions)
          print ("\t\tsize:", nc_fid.variables[var].size)
          print_ncattr(var)
    return nc_attrs, nc_dims, nc_vars

  def setNC_3D( filename = 'temp.nc'  , varname = 'Helloworld' ) :
    # Write in the NCFiles
    nc_fid = netCDF4.Dataset(Tracking.checkRequiredPath('th'),'r')
    (nc_attrs, nc_dimes, nc_vars) = Tracking.ncdump(nc_fid)
    
    for dim in nc_fid.dimensions.keys() :
      print(nc_fid.dimensions[dim].name,' ',nc_fid.dimensions[dim].size,' ' ,nc_fid.dimensions[dim].isunlimited())

    '''

    w_nc_fid = netCDF4.Dataset(filename,'w',format='NETCDF4')
    w_nc_fid.description = "Cloud Connecting Programm. Programmed by Mars, Algorithm By Willy Tsai"
    w_nc_fid.createDimension( 'Time' , None )
    
    
    w_nc_dim = w_nc_fid.createVariable('Time',nc_fid.variables['Time'].dtype,('Time'))
    
    for var_attr in nc_fid.variables['Time'].ncattrs():
      w_nc_dim.setncattr( var_attr , nc_fid.variables['Time'].getncattr(var_attr) )
    
    w_nc_fid.variables['Time'][:] = nc_fid.variables['Time'][:]
    w_nc_fid.close()
    # HERE    
    '''
  def getNC( parameter , time ) :
    # [ Time , Z , Y , X ]
    nc_data = netCDF4.Dataset(IOManager.checkRequiredPath(parameter),'r')
    var =  nc_data.variables[parameter][time][0:Parameter.NZ][0:Parameter.NY][0:Parameter.NX]
    nc_data.close()
    
    return var 

  def getNC_1D( parameter ) :
    # [ Time , X ]

    nc_data = netCDF4.Dataset(IOManager.checkRequiredPath(parameter),'r')
    var =  nc_data.variables[parameter][:]
    nc_data.close()
    
    return var 
    






