from cloud_definition import *
import sys
import re
import netCDF4
import numpy as np
import _pickle as pickle
import multiprocessing as mp
import os.path
import time as systime

# The Path and process num can be changed here
class Path:
  # The folder of NC Files
  nc_folder      = '/data2/mog/VVM/DATA/gate_shear/'

  # The Path to Processed Files
  out_path       = '/home/C.ykhuang/vvmpython/output/'

  # Output File names
  out_pickle     = 'cloud_test.pickle'
  out_acpickle   = 'cloudAC.pickle'
  out_trackHead  = 'trackedHead.pickle'
  out_graph_path = 'graph/'

  # Maximun Thread numbers
  PROCNUM   = 16 # the num of process

  def __init__(self):
    return 

# The Path of each NC Files can be changed here
class Ncpath:
  nc_path = Path.nc_folder
  p_name  = 'gat'
  postfix = 'nc'
  qc  = nc_path + p_name + 'L.qc3d.' + postfix
  qg  = nc_path + p_name + 'L.qg3d.' + postfix
  qi  = nc_path + p_name + 'L.qi3d.' + postfix
  qr  = nc_path + p_name + 'L.qr3d.' + postfix
  qs  = nc_path + p_name + 'L.qs3d.' + postfix
  qv  = nc_path + p_name + 'L.qv3d.' + postfix
  rad = nc_path + p_name + 'L.rad.'  + postfix
  th  = nc_path + p_name + 'L.th3d.' + postfix
  u   = nc_path + p_name + 'L.u3dx.' + postfix
  v   = nc_path + p_name + 'L.u3dy.' + postfix
  w   = nc_path + p_name + 'L.w3d.'  + postfix
  zdx = nc_path + p_name + 'L.z3dx.' + postfix
  zdy = nc_path + p_name + 'L.z3dy.' + postfix
  zdz = nc_path + p_name + 'L.z3dz.' + postfix
  phy = nc_path + p_name + 'C.phys.' + postfix

  def __init__(self):
    return 



# The Parameter of Tracking and Connecting can be changed here
class Parameter:

  '''Parameter List
    DX DY DZ DZ1 DT
    NX NY NZ NT
    ODT ( Output Time Lenght)
    RHO ( Average Density of each layer )
    CONNECT ( The Connected method of cloud, default 6-connected )
    MOVE ( The trying moves of each interation )
  '''

  # Manual Setting
  CONNECT = 6

  # [+DZ,+DY,+DX]
  MOVE = [  
    [  0 ,  0 , -1 ] ,
    [  0 , -1 ,  0 ] ,
    [  0 ,  0 ,  1 ] ,
    [  0 ,  1 ,  0 ] ,
    [  1 ,  0 ,  0 ] ,
    [ -1 ,  0 ,  0 ]
  ]

  #

  # Parse the Parameters from Fortran Output files.
  # TODO : parse DX ... DT from INPUT

  workpath = Path.nc_folder + 'ldinput.f' # should use vvm.setup
  fp = open(workpath,'r')
  for line in fp:
    result = re.search('\s+(\w+)\s+=\s?(\d+\.\d?)_dbl_kind',line)
    if ( result and result.group(1) == 'DX' ):
      DX = float(result.group(2))
    if ( result and result.group(1) == 'DYNEW' ):
      DY = float(result.group(2)) 
    if ( result and result.group(1) == 'DZ' ):
      DZ = float(result.group(2))
    if ( result and result.group(1) == 'DZ1' ):
      DZ1= float(result.group(2))
    if ( result and result.group(1) == 'DT' ):
      DT = float(result.group(2))
  fp.close()

  workpath = Path.nc_folder + 'PARMSLD.f90'
  fp = open(workpath,'r')
  for line in fp:
    result = re.search('\s+(\w+)\s?=\s?(\d+)\s?,',line)
    if ( result and result.group(1) == 'MI_glob' ):
      NX = int(result.group(2))
    if ( result and result.group(1) == 'MJ_glob' ):
      NY = int(result.group(2))
    if ( result and result.group(1) == 'NK2' ):
      NZ = int(result.group(2))
  fp.close()

  workpath = Path.nc_folder + 'INPUT'
  fp = open(workpath,'r')
  for line in fp:
    res0 =  re.search('ITTADD\s?=\s?(\d+)\s?/',line) 
    res1 =  re.search('NXSAVG\s?=\s?(\d+)\s?,',line) 
    if ( res0 ) :
      ntadd = int(res0.group(1))
    if ( res1 ) :
      nxsavg = int(res1.group(1))
  NT  = int (ntadd / nxsavg + 1)
  ODT = float(nxsavg) * float(DT) # Output Time Length
  fp.close()

  # Parse RHO
  RHO = []
  workpath = Path.nc_folder + 'fort.98'
  fp   = open(workpath,'r')

  line = fp.readline()  # Get the Title line
  while( line and not re.search(r"K, RHO\(K\),THBAR\(K\),",line) ):
    line = fp.readline()
  line = fp.readline()  # Omit the first line
  for k in range(NZ+1):
    line = fp.readline()
    [index,rho,thbar,pbar,pibar,qvbar] = line.split()
    RHO.append(float(rho))
  fp.close()

if __name__ == '__main__':
  print('--------VVM Parameter---------')
  print("NX=%5d, NY=%5d, NZ=%5d, NT=%6d"
    %(Parameter.NX,Parameter.NY,Parameter.NZ,Parameter.NT)
  )
  print('DX=%5d, DY=%5d, DZ=%5d, DZ1=%5d, DT=%5d, ODT=%5.1f'
    %(Parameter.DX,Parameter.DY,Parameter.DZ,Parameter.DZ1,Parameter.DT,Parameter.ODT)
  )
  print('-----END of VVM Parameter-----')

