from vvm import Tracking
from vvm import CloudPlot 
from Parameter import Parameter
from Parameter import Path
import sys
import re
import pdb
import netCDF4
import numpy as np
import _pickle as pickle
import multiprocessing as mp
import os.path
import time as systime
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns


# The process of Tracking
class ExampleForMog:

  def draw_typhoon_graph():
    trackedHead   = Tracking.readTrackedCloud() 
    fig = plt.figure()
    ax  = fig.add_subplot(111,aspect='equal')
    trackedIndex   = range(len(trackedHead)) # default select all clouds
#   Can also be specified by method like this 
#   trackedIndex   = [3,5,10] 


    # To get the ranking of the biggest volume in the life cycle of a trakced cloud
    i = 0
    for head in sorted(trackedHead , key= lambda head : head.maxVolume, reverse= True):
      print("Maximum volume of this tracked cloud is %15.5f[km^3] in index %10d " %(head.maxVolume / 1000000000.0 , trackedHead.index(head)))
      # The number below specified the number to stop, default is 20th biggest volume
      i = i + 1
      if ( i > 20 ):
        break
    

    # To get the cloud that has the longest life

    drawData = []
    for i in trackedIndex:
      head   = trackedHead[i]
      X      = []
      Y      = []
      volume = []
      while True:
        X.append(head.cloud.center_x)
        Y.append(head.cloud.center_y)
        volume.append(head.cloud.volume)
        if ( head.nexTracked == [] ):
          break
        else :
          head = head.nexTracked[0]
      drawData.append([X,Y,volume,i])
      #if (len(X) > 10 ):
      #  print ("len = %d , index = %d " %(len(X),i))
      #    drawData.append([X,Y,volume])
   # pdb.set_trace()

    i = 0
    for head in sorted(drawData , key= lambda cuDraw : len(cuDraw[0]), reverse= True):
      print("Life span of this cloud %10d is %15.5f[mins]" %(head[3] , len(head[0]) * Parameter.ODT / 60.0))
      i = i + 1
      # The number below specified the number to stop, default is 20th longest life span
      if ( i > 20 ):
        break
    
    for i in range(len(drawData)):
      # Get the i-th data, say D , in the list that you specified in trackedHead
      # The data structure 
      # 1 : [ 
      #   The center in x [x1,x2,x3,...] ,
      #   The center in y [y1,y2,y3,...] ,
      #   The volume      [v1,v2,v3,...] ,
      #   index, which is  1
      # ]
      # 2 : [ 
      #   The center in x [x1,x2,x3,...] ,
      #   The center in y [y1,y2,y3,...] ,
      #   The volume      [v1,v2,v3,...] ,
      #   index, which is 2
      # ]
      # .....


      # Draw [x,y] points of D 
      plt.plot(drawData[i][0],drawData[i][1],'r-', linewidth = 0.5 )
      
      # Draw corresponed circle of D
      for j in range(len(drawData[i][2])):
        circle = plt.Circle(
          (drawData[i][0][j],drawData[i][1][j]), # The center of the circle 
          drawData[i][2][j] ** (1/3) / (Parameter.DX ** 2 + Parameter.DY ** 2) ** 0.5, # the radius
          fill = False,
        )
        ax.add_patch(circle)

    # Label of graph
    plt.xlabel('X grid')
    plt.ylabel('Y grid')
    fig.savefig(Path.out_graph_path + 'typhoon.png', dpi=300)


if __name__ == '__main__':

  #Tracking.makeCloudConFile()
  #Tracking.track_cloud([3500,3600])
  ExampleForMog.draw_typhoon_graph()  





