# The definition of cloud object
class Cloud:
  def __init__(self):
    self.label      = 0
    self.total_grid = 0
    self.volume     = 0.0
    self.h_max      = -1.0 
    self.h_min      = 10000000000000 # Should be the height of domain 
    self.qc_sum     = 0.0
    self.qi_sum     = 0.0
    self.bouy_sum   = 0.0
    self.VKE_sum    = 0.0
    self.enstrophy  = 0.0
    self.mass_flux  = 0.0 
    self.massInt    = 0.0
    self.center_x   = 0.0
    self.center_y   = 0.0
    self.center_z   = 0.0
    self.u_average  = 0.0
    self.v_average  = 0.0
    self.proArea    = 0.0
    
    def __hash__(self):
      return hash((self.label, self.volume, self.center_x, self.center_y))

    def __eq__(self, other):
      return (
           (self.label , self.volume , self.center_x , self.center_y ) 
        == (other.label, other.volume, other.center_x, other.center_y)
      )
    def __ne__(self, other):
      return not(self == other)


class SuperCloud:
  def __init__(self):
    self.cores      = []
    self.label      = 0
    self.total_grid = 0
    self.volume     = 0.0
    self.h_max      = -1.0 
    self.h_min      = 10000000000000 # Should be the height of domain 
    self.qc_sum     = 0.0
    self.qi_sum     = 0.0
    self.bouy_sum   = 0.0
    self.VKE_sum    = 0.0
    self.enstrophy  = 0.0
    self.mass_flux  = 0.0 
    self.massInt    = 0.0
    self.center_x   = 0.0
    self.center_y   = 0.0
    self.center_z   = 0.0
    self.u_average  = 0.0
    self.v_average  = 0.0
    self.proArea    = 0.0


class TrackedCloud:
  def __init__(self):
    self.cloud = None
    self.time  = -1
    self.head  = [] # Indicate the head of this track, [] if current is head
    self.nexTracked = []
    self.preTracked = []
    
    self.maxVolume  = 0.0


    
